jQuery UI Maven Artifact
------------------------

The [jQuery UI JavaScript library][1] packaged as a [Maven][2] artifact
suitable for the [JavaScript Maven Plugin][3].

Contains the minimized and the uncompressed JavaScript code of the plugin
and an externs file for [Google's Closure Compiler][4].

[1]: http://jqueryui.com "jQuery UI JavaScript Library"
[2]: http://maven.apache.org/ "Apache Maven"
[3]: https://github.com/kayahr/javascript-maven-plugin "JavaScript Maven Plugin"
[4]: https://developers.google.com/closure/compiler/ "Google Closure Compiler"
